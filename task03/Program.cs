﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task03
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a single digit whole number");
            number(Console.ReadLine());
        }

        static void number(string a)
        {
            int input = int.Parse(a);
            Console.WriteLine($"{input} + {input}");
            Console.WriteLine($"{input} - {input}");
            Console.WriteLine($"{input} / {input}");
            Console.WriteLine($"{input} * {input}");

        }
    }
}
